package hello;

import org.joda.time.LocalTime;

public class HelloWorld {
    public static void main(String[] args) {

        Greeter greeter = new Greeter();
        System.out.println(greeter.sayHello());
        
        System.out.println("The planned expansion locations include: Earth's Moon, Mars, Pluto, Saturn, Jupiter");        
    }
}
